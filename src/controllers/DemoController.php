<?php
class DemoController extends Controller
{
    public $productModel;
    public function __construct($request)
    {
        parent::__construct($request);
        $this->productModel = new ProductModel();
    }
  

    public  function Get()
    {
        return OK($this->productModel->where('name','LIKE','%a%','s')->limit(0,2)->get());
    }
}

