<?php
define('DB_HOST','127.0.0.1');
define('DB_USER','root');
define('DB_PASS','');
define('DB_NAME','test');
define('DB_PORT','3306');
define('NAV_BAR', ROOT_DIR.'/src/views/nav-bar.php');
define('FOOTER', ROOT_DIR.'/src/views/footer.php');
define('PAGINATION', ROOT_DIR . '/src/views/pagination.php');
define('CATALOG_PER_PAGE', 9);
define('BLOG_PER_PAGE', 3);
define('CORS_ORGIN', '*');
define('CORS_HEADER', '*');
